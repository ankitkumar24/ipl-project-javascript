const fs = require('fs');
const csv = require('csv-parser');

// const MATCHES_FILE_PATH = 'src/data/matches.csv';
const DELIVERY_FILE_PATH = 'src/data/deliveries.csv';

const dismissedPlayer = {};
let topDismissedPlayer={};

// Read matches.csv file
fs.createReadStream(DELIVERY_FILE_PATH)
    .pipe(csv())
    .on('data', (deliveryRow) => {
        const dismissed_player = deliveryRow.player_dismissed;
        const bowler = deliveryRow.bowler;
        
        if (dismissed_player.length > 0) {
            if(!dismissedPlayer[bowler]){
                dismissedPlayer[bowler]=[];
            }
            if (dismissedPlayer[bowler][dismissed_player]) {
                dismissedPlayer[bowler][dismissed_player] += 1;
            }
            else {
                dismissedPlayer[bowler][dismissed_player] = 1;
            }
        }


    })

    .on('end', () => {
        // console.log(dismissedPlayer);
        for(bowler in dismissedPlayer){

        const entries = Object.entries(dismissedPlayer[bowler]);

        // Sort the array based on the values in asscending order
        entries.sort((a, b) => b[1] - a[1]);

        // Get only the top 10 elements
        const top1 = entries.slice(0, 1);

        // Convert the top 10 array back into an object
         topDismissedPlayer[bowler] = Object.fromEntries(top1);
    }


        const jsonData = JSON.stringify(topDismissedPlayer, null, 2);

        // Write the JSON data to a file
        fs.writeFile('src/public/output/mostDismissedPlayer.json', jsonData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing JSON file:', err);
                return;
            }
            console.log('Data written to JSON file successfully.');
        });

        // console.log(topDismissedPlayer);
    });

