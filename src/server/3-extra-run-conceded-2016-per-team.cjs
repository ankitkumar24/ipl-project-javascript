const fs = require('fs');
const csv = require('csv-parser');

const MATCHES_FILE_PATH = 'src/data/matches.csv';
const DELIVERY_FILE_PATH = 'src/data/deliveries.csv';

const extraRunPerTeam = {};
const matchId2016 = [];

// Read matches.csv file
fs.createReadStream(MATCHES_FILE_PATH)
  .pipe(csv())
  .on('data', (matchRow) => {
    const year = matchRow.season;
    if (year === '2016') {
      matchId2016.push(matchRow.id);
    }
  })
  .on('end', () => {
    // Read deliveries.csv file
    fs.createReadStream(DELIVERY_FILE_PATH)
      .pipe(csv())
      .on('data', (deliveryRow) => {
        const matchId = deliveryRow.match_id;
        const team = deliveryRow.bowling_team;
        const extraRuns = parseInt(deliveryRow.extra_runs);

        if (matchId2016.includes(matchId)) {
          if (extraRunPerTeam[team]) {
            extraRunPerTeam[team] += extraRuns;
          } else {
            extraRunPerTeam[team] = extraRuns;
          }
        }
      })
      .on('end', () => {
        const jsonData = JSON.stringify(extraRunPerTeam, null, 2);

        // Write the JSON data to a file
        fs.writeFile('src/public/output/extraRunPerTeam.json', jsonData, 'utf8', (err) => {
          if (err) {
            console.error('Error writing JSON file:', err);
            return;
          }
          console.log('Data written to JSON file successfully.');
        });

        console.log(extraRunPerTeam);
      });
  });
