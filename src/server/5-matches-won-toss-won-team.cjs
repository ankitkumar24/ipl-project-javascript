const fs = require('fs');
const csv = require('csv-parser');

const MATCHES_FILE_PATH = 'src/data/matches.csv';
const matchesWon = {};

// Read matches.csv file
fs.createReadStream(MATCHES_FILE_PATH)
    .pipe(csv())
    .on('data', (matchRow) => {
        const winner = matchRow.winner;
        const toss_winnner = matchRow.toss_winner;
        if (winner === toss_winnner) {
            if (matchesWon[winner]) {
                matchesWon[winner] += 1;
            }
            else {
                matchesWon[winner] = 1;
            }
        }
    })
    .on('end', () => {

        const jsonData = JSON.stringify(matchesWon, null, 2);

        // Write the JSON data to a file
        fs.writeFile('src/public/output/matchesTossWon.json', jsonData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing JSON file:', err);
                return;
            }
            console.log('Data written to JSON file successfully.');
        });

        console.log(matchesWon);
    });

