const fs = require('fs');
const csv = require('csv-parser');

const MATCHES_FILE_PATH = 'src/data/matches.csv';
const DELIVERY_FILE_PATH = 'src/data/deliveries.csv';

const top10EconomicalBowler = {};
const matchId2015 = [];
const bowler_run = {};
const bowler_ball = {};

// Read matches.csv file
fs.createReadStream(MATCHES_FILE_PATH)
    .pipe(csv())
    .on('data', (matchRow) => {
        const year = matchRow.season;
        if (year === '2015') {
            matchId2015.push(matchRow.id);
        }
    })
    .on('end', () => {
        // Read deliveries.csv file
        fs.createReadStream(DELIVERY_FILE_PATH)
            .pipe(csv())
            .on('data', (deliveryRow) => {
                const matchId = deliveryRow.match_id;
                const bowler = deliveryRow.bowler;
                const bowlerRuns = (parseInt(deliveryRow.total_runs) - parseInt(deliveryRow.extra_runs));

                if (matchId2015.includes(matchId)) {
                    if (bowler_run[bowler]) {
                        bowler_run[bowler] += bowlerRuns;
                        bowler_ball[bowler] += 1;
                    } else {
                        bowler_run[bowler] = bowlerRuns;
                        bowler_ball[bowler] = 1;
                    }

                }


            })
            .on('end', () => {
                for (key in bowler_run) {
                    top10EconomicalBowler[key] = (bowler_run[key] / (parseInt(bowler_ball[key] / 6)));
                }
                const entries = Object.entries(top10EconomicalBowler);

                // Sort the array based on the values in descending order
                entries.sort((a, b) => a[1] - b[1]);

                // Get only the top 10 elements
                const top10 = entries.slice(0, 10);

                // Convert the top 10 array back into an object
                const top10Bowler = Object.fromEntries(top10);


                const jsonData = JSON.stringify(top10Bowler, null, 2);

                // Write the JSON data to a file
                fs.writeFile('src/public/output/top10EconomicalBowler.json', jsonData, 'utf8', (err) => {
                    if (err) {
                        console.error('Error writing JSON file:', err);
                        return;
                    }
                    console.log('Data written to JSON file successfully.');
                });

                // console.log(top10Bowler);
            });
    });
